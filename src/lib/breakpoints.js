import Vue from 'vue';

let timeout;

const getBreakpoint = (width) => {
	if (width >= 1280) return 'xl';
	if (width >= 1024) return 'lg';
	if (width >= 768) return 'md';
	if (width >= 640) return 'sm';
	return 'xs';
};

// Create a reactive object
const breakpoints = Vue.observable({
	is: getBreakpoint(window.innerWidth),
});

window.addEventListener(
	'resize',
	() => { // debounce and avoid event handler miss on target 
		clearTimeout(timeout);
		timeout = setTimeout(function(){
			breakpoints.is = getBreakpoint(window.innerWidth);
		}, 100);
	},
	false,
);

export default breakpoints;