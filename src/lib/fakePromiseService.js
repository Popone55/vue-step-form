export default {
	async submitData(data) {
		await new Promise(resolve => setTimeout(resolve, 2500));
		const rand = Math.floor(Math.random() * 3); // 1 possibilità su tre di errore
		if(rand === 0) {
			throw(new Error('Validazione fallita!'));
		}
		return data;
	},
};