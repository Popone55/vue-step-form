
export default {
	basic(data) {
		return !!data;
	},

	email(data) {
		// eslint-disable-next-line no-useless-escape
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return !!data && re.test(String(data).toLowerCase());
	},

	fiscalCode (data) {
		const reFiscal = /^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i;
		const rePiva = /^[0-9]{11}$/;
		return !!data && (reFiscal.test(String(data).toLowerCase()) || rePiva.test(String(data).toLowerCase()));
	},

	number(data) {
		const number = parseInt(data);
		return !!number && Number.isInteger(number) && number > 0 && number <= 122; // https://en.wikipedia.org/wiki/List_of_the_verified_oldest_people#:~:text=The%20oldest%20verified%20man%20ever,aged%20112%20years%2C%20234%20days.
	},
};