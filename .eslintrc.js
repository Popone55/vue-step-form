module.exports = {
	root: true,
	env: {
		browser: true,
		node: true,
	},
	parserOptions: {
		parser: 'babel-eslint',
	},
	extends: [
		'plugin:vue/recommended',
		'eslint:recommended',
	],
	rules: {
		'quotes': ['error', 'single'],
		'semi': ['error', 'always'],
		'indent': ['warn', 'tab', {
			'SwitchCase': 1,
			'MemberExpression': 2,
		}],
		'space-before-function-paren': 'off',
		'no-tabs': 'off',
		'brace-style': ['error', 'stroustrup'],
		'comma-dangle': [
			'warn', {
				'arrays': 'always-multiline',
				'objects': 'always-multiline',
				'imports': 'always-multiline',
				'exports': 'always-multiline',
				'functions': 'always-multiline',
			},
		],
		'vue/html-indent': ['warn', 'tab', {
			'baseIndent': 1,
			'attribute': 2,
		}],
		'vue/html-closing-bracket-newline': ['warn', {
			'multiline': 'always',
			'singleline': 'never',
		}],
		'template-curly-spacing': ['error', 'always'],
		'no-console': 'off',
		'no-debugger': 'off',
		'vue/require-component-is': 'off',
		'spaced-comment': 'warn',
	},
};
